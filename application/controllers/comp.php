<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comp extends CI_Controller {

	public $debug = FALSE;
	public $hunters;
	private $mice;
	private $customer = '';
	private $comp_mice;
	private $categories;
	
	public function __construct() {
		parent::__construct();
		$this->load->library( array('curl', 'session', 'formbuilder', 'jquery_ext') );
		$this->load->helper( array('url', 'general', 'language') );
		
		$this->customer = $this->session->userdata('user');
		
		if($this->customer != 'bama') redirect('main/login');
		
		//load mice array
		$this->set_mice_array();
		
		//load comp mice
		$this->set_comp_mice();
		
		$this->hunters = $this->parse_csv( APPPATH . 'comp2.csv' );
		
		log_message('debug', "Main Controller Class Initialized");
	}
	
	public function index() {

		if($this->input->server("REQUEST_METHOD") == "POST") {
			echo "KUL!";
		}
		else {
			
			$this->benchmark->mark('code_start');
			//$horns = $this->horns_tracker();
			$this->benchmark->mark('code_end');
			
			//$data = $this->curl('http://www.mousehuntgame.com/profile.php?snuid=543464846');
			
			$this->output('main/comp_view', '20 vs 20 comp', array(), FALSE);
		}
		
	}
	
	public function horns() {
		$horns = $this->horns_tracker();
		$this->output('main/horns_view', 'UBOTS', array('horns' => $horns), FALSE);
	}
	
	public function horns_tracker() {
		
		$return = array();
		
		if(!isset($this->simple_html_dom)) $this->load->library('simple_html_dom');
		
		foreach($this->hunters as $hunter) {
			
			$html = new simple_html_dom();
			$url = 'http://www.mousehuntgame.com/profile.php?snuid='.$hunter['snuid'];
			//$html->load_file($url);
			$html->load($this->curl($url));
			$info = $html->find('div[class=campLeft]',0)->plaintext;
			
			//dump2($info);
				
			$return[] = array(
					'name' 			=> $hunter['name'],
					'id'			=> $hunter['snuid'],
					'horn_calls'	=> $this->extractString('Calls:', 'Location', $info),
					'mice_caught'	=> $this->extractString('Mice Caught:', '', $info),
					'title'			=> $this->extractString('Title:', 'Points', $info)
					);
		}
		
		//echo json_encode($return);
		return $return;
	
	}
	
	public function mice($start=1) {
		$start = (int) $start;
		$this->benchmark->mark('code_start');
		$this->parser($start);
		$this->benchmark->mark('code_end');
	}
	
	
	protected function extractString($needle1, $needle2, $string, $pos1=0) {
	
		$padding1 = strlen($needle1);
		$padding2 = strlen($needle2);
	
		$pos1 = strpos($string, $needle1, $pos1) + $padding1;
		if(!($pos1 === false)) {
			if($needle2 != '') $pos2 = strpos($string, $needle2);
			else $pos2 = strlen($string);
			$iskaniNiz = html_entity_decode(trim(substr($string, $pos1, $pos2 - $pos1)));
		}
		return $iskaniNiz;
	}

	public function parser($page=1) {
		
		$this->validate();
		
		$page = (int) $page;
		$this->benchmark->mark('code_start');

		$this->load->library('table');
		
		$tmpl = array ( 'table_open' => '<table class="table table-striped">' );
		$this->table->set_template($tmpl);
		
		$heading = array('#', 'Name');
		$heading_mice = array(0 => '', 1 => '');
		
		//we have IDs but we want mice names
		foreach($this->comp_mice as $key) {
			$cellKeys[] = $this->mice[$key];
			$heading[] = '<img src="'.images_url().'mice/'. $key.'.gif" alt="'.$this->mice[$key].'" title="'.$this->mice[$key].'" width="50px" height="50px" />';
			$heading_mice[] = $key;
		}
		
		$this->table->set_heading($heading);
		
		//get saved data for current customer
		$snuids = array();

		//slice
		//$output = array_slice($this->hunters, $page, 20);
		
		list($pages, $pagination, $output) = $this->paging($this->hunters, $page);
		
		//collect data only for current page
		foreach($output as $count => $hunter) {
			
			$snuids[] = $snuid = $hunter['snuid'];
			
			$parsed_data = array();
			
			//###### PARSING AREA ##################
			
			foreach($this->categories as $cat_id => $cat) {
				
				$url = "https://www.mousehuntgame.com/managers/ajax/mice/getstat.php?action=getMiceByGroup&fetch_categories=false&category=$cat&snuid=$snuid&uh=x0D6F716&hg_is_ajax=1&sn=FBConnect";
				
				//echo "$snuid | $cat_id | $cat".BR;
				
				$data = $this->execute($cat, $snuid);
				
				if(!is_object($data) || is_null($data)) continue;
				
				$mice = get_object_vars($data->miceStat);
				
				
				foreach($mice as $id => $mouse) {
					
					//najdi opisno ime
					$name_long = (string) trim($mouse->name);
					
					//najdi kratko ime v listi vseh misi
					$name_short = array_search($name_long, $this->mice);
					
					if($name_short !== FALSE && in_array($name_short, $this->comp_mice)) {
						$parsed_data[$name_short] = (string) $mouse->caught .' / '. (string) $mouse->missed;
					}
				}
				
				//TESTING
				//if($cat_id > 0) break;
			}
			
			//############## DISPLAY AREA ###################
			
			$row = array();
			//add rows
			for($i=0; $i<count($heading); $i++) {

				if($i == 0) {
					$row[] = array('data' => $count);
				}
				else if($i==1) {
					$row[] = (array('data' => anchor($hunter['link'], $hunter['name'], array('target' => 'blank', 'title' => 'open hunter link in new window'))));
				}
				else {
					$pointer = $heading_mice[$i];
					$val = (isset($parsed_data[$pointer])) ? $parsed_data[$pointer] : 'n/a';
					$row[] = array('data' => $val);
				}
				
			}
			
			$this->table->add_row($row);
			
			//TESTING
			//if($count > 0) break;
			
		}
		
		$table = $this->table->generate();
		
		$outData = array(
				'table' => $table,
				'snuids'=> implode(',', $snuids),
				'pages'	=> $pages,
				'page'	=> $page,
				'pagination'	=> $pagination
		);
		
		$this->benchmark->mark('code_end');
		
		$this->output('main/parser2', 'Parser FTC', $outData, false );
	}
	
	private function paging($data, $page) {
		$max = count($data);
		$per_page = PAGE_NUM;
		$page = (int) $page;
		
		//echo "PAGE: ".$page.BR;
		//echo "MAX: ".$max.BR;
		
		//create pagination
		if($max > $per_page) {
		
			$func = site_url() . 'comp/parser/';
		
			$pages = ceil($max / $per_page);
			//echo "PAGES: ".$pages.BR;
		
			//error handling: prevent manual access to bigger page than possible
			if($page == 0 || $page > $pages) {
				show_error('Page number is out of limits.', 405);
			}
		
			$remainder = $max % $per_page;
		
			$all_page_data = array_chunk($data, $per_page);
		
			//extract data for current page only
			$page_data = $all_page_data[$page-1];
		
			$pagination = '<div class="pagination pagination-centered">'."\n";
			$pagination .= '<ul>'."\n";
		
			//'.$previous.'
			if($page == 1) $pagination .= '<li class="disabled"><a href="#">Previous</a></li>'."\n";
			else $pagination .= '<li><a href="'.$func.($page-1).'">Previous</a></li>'."\n";
		
			for($i=1; $i<$pages+1; $i++) {
					
				if($i == $page) $pagination .= '<li class="active"><a href="#">'.$i.'</a></li>'."\n";
				else $pagination .= '<li><a href="'.$func.$i.'">'.$i.'</a></li>'."\n";
			}
		
			if($page == $pages) $pagination .= '<li class="disabled"><a href="#">Next</a></li>'."\n";
			else $pagination .= '<li><a href="'.$func.($page+1).'">Next</a></li>'."\n";
		
			$pagination .= '</ul>'."\n";
			$pagination .= '</div>'."\n";
		
		}
		else {
			$page_data = $data;
			$pages = 1;
			$pagination = '';
		}
		
		return array($pages, $pagination, $page_data);
	}
	
	private function proxy_post($cat, $snuid)
	{
		$url = "https://www.mousehuntgame.com/managers/ajax/mice/getstat.php";
		
		$fields = array(
				"sn"				=> "Hitgrab",
				"hg_is_ajax"		=> 1,
				"action"			=> "getMiceByGroup",
				"fetch_categories"	=> 0,
				"category"			=> $cat,
				"snuid"				=> $snuid,
				"uh"				=> "nWjbx1HS"
				);
		
		$headers = array(
				"Accept"		=> "application/json, text/javascript",
				"Connection"	=> "keep-alive",
				"Content-Type"	=> "application/x-www-form-urlencoded",
				"Cookie"		=> 'PHPSESSID=8a5rbjuu34ls6sboje37ijtoo4; ki_u=8ce176cf-0d56-9da4-f064-2c01b980c704; hg_session[startTime]=1389776567; hg_session[sessionId]=DM5h36uIye2PMRQ60U6829JvYZc4CCgf; hg_session[sessionNum]=6; ki_t=1389814456175%3B1389814456175%3B1389829489439%3B1%3B13; __utma=22815271.858546367.1389814456.1389814456.1389814456.2; __utmb=22815271.39.10.1389816233; __utmc=22815271; __utmz=22815271.1389814456.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
		);
		
		$fields_string = http_build_query($fields, NULL, '&');
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		curl_setopt($ch,CURLOPT_URL, $url);
		curl_setopt($ch,CURLOPT_POST, TRUE );
		curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, TRUE);
		//curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, FALSE);
		
		curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, TRUE);
		curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch,CURLOPT_CAINFO, data_url() . 'cacert.pem');
		
		//curl_setopt($ch, CURLOPT_COOKIEJAR, data_url() . 'cookie.txt');
		//curl_setopt($ch, CURLOPT_COOKIEFILE, data_url() . 'cookie.txt');
		
		//die('sranje');
		
		$result = curl_exec($ch);
		
		$info = curl_getinfo($ch);
		$err_code = curl_errno($ch);
		$error = curl_error($ch);
		
		echo "<h2>INFO</h2>";
		dump2($info);
		echo "<h2>ERROR</h2>";
		dump2($error);
		
		curl_close($ch);
		return $result;
	}
	
	private function execute($cat, $snuid) {
	
		$url = "https://www.mousehuntgame.com/managers/ajax/mice/getstat.php?action=getMiceByGroup&fetch_categories=false&category=$cat&snuid=$snuid&uh=x0D6F716&hg_is_ajax=1&sn=FBConnect";
	
		$headers = array(
				"Accept"	=> "text/html",
				"Cookie"	=> 'PHPSESSID=8a5rbjuu34ls6sboje37ijtoo4; ki_u=8ce176cf-0d56-9da4-f064-2c01b980c704; hg_session[startTime]=1389776567; hg_session[sessionId]=DM5h36uIye2PMRQ60U6829JvYZc4CCgf; hg_session[sessionNum]=6; ki_t=1389814456175%3B1389814456175%3B1389826213533%3B1%3B11; __utma=22815271.858546367.1389814456.1389814456.1389814456.2; __utmb=22815271.30.10.1389816233; __utmc=22815271; __utmz=22815271.1389814456.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
		);
	
		foreach($headers as $name => $content) {
			$this->curl->http_header($name, $content);
		}
	
		$a = $this->curl->ssl(TRUE, 2, data_url() . "cacert.pem");
	
		$this->curl->create($url);
		$raw = $this->curl->execute();
		
		//$this->curl->debug();
	
		$data = json_decode($raw);	//user, messageData, badges, favorites, mouse_data, remaining_mice, is_viewing_user, success
		return $data;
	}

	private function curl($url, $ssl = FALSE ) {
	
		if(!isset($url) || $url=='') return '';
	
		if(!isset($this->curl)) $this->load->library('curl');
	
		$c = $this->curl;
		$c->create($url);
	
		$c->option(CURLOPT_FAILONERROR, true);
		$c->option(CURLOPT_CONNECTTIMEOUT, 15); //The number of seconds to wait while trying to connect. Use 0 to wait indefinitely.
		$c->option(CURLOPT_TIMEOUT, 15);	//The maximum number of seconds to allow cURL functions to execute.
		$c->option(CURLOPT_RETURNTRANSFER, TRUE);
	
		if($ssl) $a = $this->curl->ssl(TRUE, 2, data_url() . "cacert.pem");
	
		//special options
		$c->option(CURLOPT_TIMECONDITION, CURL_TIMECOND_IFMODSINCE);
		$response = $c->execute();
		if($this->debug) {
			$c->debug();
		}
		return $response;
	}
	
	private function output($pageName, $pageTitle='CMS', $data=array(), $include_menu=true) {
	
		$data['title'] = $pageTitle;
	
		//nafilamo podatke za globalni meni. Ce imamo breadcrumbe, jih prikazemo (po levelih)
		$menuData = array(
				'title' 		=> $pageTitle,
				'user_group'	=> '',
				'breadcrumbs' 	=> (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : 0
		);
	
		$this->load->view('template/header', array('title' => $pageTitle));
		if($include_menu) $this->load->view('template/menu', $menuData );
		$this->load->view($pageName, $data);
		$this->load->view('template/footer', array('page' => $pageName));
	}
	
	//init func
	private function set_mice_array() {
		
		if (defined('ENVIRONMENT') AND file_exists(APPPATH.'config/'.ENVIRONMENT.'/bama_mice.php')) {
		    include(APPPATH.'config/'.ENVIRONMENT.'/bama_mice.php');
		}
		elseif (file_exists(APPPATH.'config/bama_mice.php')) {
			include(APPPATH.'config/bama_mice.php');
		}

		//put into global
		$this->mice = (isset($mice) AND is_array($mice)) ? $mice : array();
	}
	
	private function set_comp_mice() {
		
		$this->comp_mice = array(
				'dragon', 'draconic_warden', 'whelpling'
				);
		
		//dodatno: common, gauntlet, forest, shadow, furoma, hydro, elub, nerg, derr, dracaonic, desertmarket, zzlibrary, living_garden, lost_city, sand_dunes, event
		//'dread', 'dirt', 'forgotten', 'balack', 'seasonal', 'chess', 'desertarmy', 'iceberg'
		$this->categories = array('dracaonic');
	}
	
	private function parse_csv($file) {
	
		//http://php.net/manual/en/function.fgetcsv.php
		$row = 1; $return = array();
		if (($handle = fopen($file, "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				if(isset($data[0]) && isset($data[1]))
					$return[] = array( 'name' => $data[0], 'link' => $data[1], 'snuid' => substr($data[1], strpos($data[1], '=')+1) );
				
				$row++;
			}
			fclose($handle);
		}
		return $return;
	}
	
	private function validate() {
		$logged_in = $this->session->userdata('logged_in');
		if($logged_in !== true) {
			redirect('main/login');
		}
	}

	
	
	
	
	
	
	
	
	
	
	
	

}

/* End of file comp.php */
/* Location: ./application/controllers/comp.php */
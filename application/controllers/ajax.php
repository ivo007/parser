<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Ajax extends CI_Controller {

	private $customer = '';
	private $customerData = array();
	
	//$url = 'https://www.mousehuntgame.com/managers/ajax/mice/getstat.php?action=getMiceByGroup&fetch_categories=false&category=dracaonic&snuid=538207153&uh=x0D6F716&hg_is_ajax=1&sn=FBConnect';
	public function __construct() {
		parent::__construct();
		$this->load->helper( array('url', 'general') );
		$this->load->library( array('curl', 'session') );

		//if($this->session->userdata('logged_in') !== TRUE) exit();

		$this->user = $this->customer = $this->session->userdata('user');
		$this->crowns = $this->session->userdata('crowns');
		$this->actions = $this->session->userdata('actions');
		$this->collectibles = $this->session->userdata('collectibles');
		//$this->customerData = $this->init();
	}
	
	public function get_data($snuids, $start=FALSE) {
		
		//decode urlencoded string of concatenated snuIDs
		$snuidArr = explode(',', urldecode($snuids));
		
		//TEMP ZA LUCIO
		/*
		[name] => Acolyte Mouse
		[caught] => 345
		[missed] => 631
		[thumb] => https://www.mousehuntgame.com/images/mice/thumb/a672a508c1007c9741f83faeb438f2a4.gif?cv=207
		[avg_weight] => 10 oz.
		[heaviest] => 1 lb. 4 oz.
		*/
		//$this->lucia($snuidArr); return;
		
		
		$data = array(); $i = 0;
		foreach($snuidArr as $snuid) {
			
			//parser only if customer wants it
			$crowns = (in_array('crowns', $this->actions)) ? $this->get_crowns($snuid) : array();
			$items = (in_array('collectibles', $this->actions)) ? $this->get_collectibles($snuid): array();
				
			$data[] = array('snuid' => $snuid, 'crowns' => $crowns, 'items' => $items);
			$i++;
		}
	
		header('Content-Type: application/json charset=utf-8');
		echo json_encode( array('status' => 'ok', 'data' => $data) );
		
		if($this->user != 'test') {
			//after output has been sent, lets save data to database:
			$this->load->model('mh_db');
			
			//overloadali bomo tole funkcijo
			$this->mh_db->save_data($data, $this->user, $start);
			
			//after day1:
			//$this->mh_db->save_giveaway($data, trim($this->user), 'current');
			
			//total se vedno shranjuje
			//$this->mh_db->save_giveaway($data, trim($this->user), 'total');
		}
		
	}
	
	public function get_start_data($snuids) {
		$this->get_data($snuids, TRUE);
	}
	
	public function get_collectibles($snuid, $all=false) {
		
		$data = $this->execute('items', $snuid);
		$items_data = $data->items;
				
		$items = get_object_vars($items_data);
		
		$return = array();
		foreach ($items as $key => $itemsArr) {
			
			//weapons, bases, maps, collectibles, skins
			foreach ($itemsArr as $itemObj) {
				
				//we dont want those that are not yet obtained by the hunter
				//if($itemObj->quantity === 0 || $itemObj->quantity === '0') continue;
				
				if($all || in_array($itemObj->type, $this->collectibles)) {
					
					$return[$key][] = array(
							'name'	=> $itemObj->name,
							'type'	=> $itemObj->type,
							'thumb'	=> $itemObj->thumb,
							'quantity'	=> $itemObj->quantity,
							'limited'	=> $itemObj->limited
					);
				}
			}
			
		}
		
		return $return;
	}
	
	public function get_crowns($snuid, $all=false) {
		
		$data = $this->execute('badges', $snuid);
		
		//echo "<pre>"; print_r($data); echo "</pre>"; die();
		
		$mice = get_object_vars($data->mouse_data);
		
		/*
		[name] => Grave Robber Mouse
		[thumb] => https://www.mousehuntgame.com/images/mice/thumb/636dc055355d0a4dc4124791ba9ad273.gif?cv=165
		[type] => grave_digger
		[id] => 337
		[num_catches] => 63
		*/
		$return = array();
		foreach($mice as $key => $mouseObj) {
			$hunters_mice[$mouseObj->name] = $mouseObj->num_catches;
			if($all || in_array($mouseObj->type, $this->crowns)) {
				$return[] = array($mouseObj->name, $mouseObj->type, $mouseObj->num_catches);
			}
		}
		
		return $return;
	}
	
	public function get_catches($str) {
		$startsAt = strpos($str, "(") + strlen("(");
		$endsAt = strpos($str, ")", $startsAt);
		return substr($str, $startsAt, $endsAt - $startsAt);
	}
	
	private function execute($action, $snuid) {

		$url = "http://www.mousehuntgame.com/managers/ajax/users/profiletabs.php?action=$action&snuid=$snuid";
		
		//if($this->user == 'lucia') $url = "https://www.mousehuntgame.com/managers/ajax/mice/getstat.php?action=getMiceByGroup&fetch_categories=false&category=forgotten&snuid=$snuid&uh=x0D6F716&hg_is_ajax=1";
		
		$headers = array(
				"Accept"			=> "text/html",
				//"Cookie" => "PHPSESSID=l9kpv9bp0bvni5kdd51hmcbso6; ki_u=ff5abf19-0127-6436-c4a0-4e69a7ee74c1; fbm_10337532241=base_domain=.mousehuntgame.com; ki_r=https%3A//apps.facebook.com/mousehunt/%3Ffb_source%3Dbookmark_apps%26ref%3Dbookmarks%26count%3D0%26fb_bmpos%3D2_0; hasprevauthed=1351938095; bb_thread_lastview=f62eaba4c6eee11982f3a995fee0f21ad65cb7dea-3-%7Bi-77518_i-1350554758_i-77793_i-1350833353_i-79816_i-1352056348_%7D; bb_sessionhash=80c9105364340c09f59b25de099ce294; hgRefHash=pS5dRUtOn1r9d4O0x5xTExV0w9JWW9fh; weapon=high_tension_spring_weapon; switch_to=standard; fbsr_10337532241=rdQI4QVula1BGcZyuJR4f4VTGlzT7zLiXzIp5H49-kM.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImNvZGUiOiJBUUNKeU5yMHVCeHNCUkRRSHAwdlpWRDB6MmFSOXBsa092b0xsdWxMZ1d6bzhXWlBkLXlJOWNaVHNsbDFBeTBVeGNrMDNOSEdIY3dyRzVoMUJHZ0FSbTBUMEdDUjh4MGJQR19YSHh2QTdMM3ZyRDBTU2QxNnNsdDR5TlJveUxwWUtJcW0wZkJENzhtY3JaejVWT0pRVi1VTTI4eUxpa3hMMUJYMmlPRVp5RWtMdDRXRW42NDBPOFZ3QWVHRzJCdDJCamhGQnFkSzZSaHBlTnhSZVU2UUNwNW8iLCJpc3N1ZWRfYXQiOjEzNTQxMzM0ODEsInVzZXJfaWQiOiI1MzgyMDcxNTMifQ; hg_session[startTime]=1354133461; hg_session[sessionId]=jVf3Lb5k3CUF85XZbL4Z0XHUK2fsDXDM; hg_session[sessionNum]=53; ki_t=1350142065910%3B1354123686665%3B1354133827105%3B82%3B3283"
				//"Cookie" => "PHPSESSID=v492jt884alu4pfv67u66cob97; ki_u=054db39f-cd69-4231-8753-cc10990c10a1; login_token=e69fa4b93be62075554e76e42ec968a7%7C538207153; ki_r=http%3A//horntracker.com/loot.php%3Floot_id%3D976; hgRefHash=3H0iAAl051n9BqhwrOApTuNhq9fA27Cg; fbm_10337532241=base_domain=.mousehuntgame.com; bb_thread_lastview=4507e80b5358eed4502e61d07b12fcbc1ef73782a-1-%7Bi-90602_i-1366288580_%7D; bb_sessionhash=dd78041e6c03fa17ed5abeae312c6aeb; hg_session[startTime]=1366355922; hg_session[sessionId]=BBO14JaTwk0J1iV1099u8DIy3s3Nx4lU; hg_session[sessionNum]=14; ki_t=1365892544612%3B1366328822972%3B1366363414654%3B10%3B323; __utma=22815271.125772452.1365892546.1366308138.1366355935.22; __utmb=22815271.27.10.1366355935; __utmc=22815271; __utmz=22815271.1365892546.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)"
				//"Cookie" => "PHPSESSID=mffrr3auc8vjf57j0i0ngt9ko4; ki_u=3f4bff47-bc8f-c4d9-e092-d6ae426beb6e; login_token=e69fa4b93be62075554e76e42ec968a7%7C538207153; ki_r=https%3A//www.facebook.com/l.php%3Fu%3Dhttps%253A%252F%252Fwww.mousehuntgame.com%252Fitem.php%253Fitem_type%253Dmap_clue_stat_item%26h%3D8AQGPme5G; fbm_10337532241=base_domain=.mousehuntgame.com; bb_sessionhash=d5e70e62d2028e2c67da1cf89c9ea3c8; fbsr_10337532241=jT0fy--8RnF3X5xPU0yz_OgxgvVkMjH-GniANGmEgIU.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImNvZGUiOiJBUUR5WXlOWjFVczZ1MHN5Mjc0bV9qWkVlSnFkUTd3ZE1ObGh4cF9WRUc3M2g3Z25Db1pJR2NWUUtJdUt1aUdVUG9GQWlyWFhyR0ZlRDVQb0JPZl9WN2MwSjR3em1RSkpiV24ybmJOOXlTRk00YUlWUXU1R3I5RFl5TWdOaFFQZUhITTR1MWNJTmx6clhPNGdDbVZPUXF3ckJFd0hEcEtpaTdNVGFYT01tRHFuWkEyWGhfcHc4UC1JS0RYVlFIbS1wb0diN1dsbDFPeGstbHNIeF9KQi12dXVzVGpMbWlkb3JDaXFXdmJWN2U2OXV4Tk1RRG1aelF0R1p6MElLeUN1WFVkcEtlR1dKZ2ltS2RoX2RvNm94SmE0REFucTFSNFdkVVVOQlNaY00zc0ZjeFRTRlB3dlhNRU5fZEs4M1VkREZpRSIsImlzc3VlZF9hdCI6MTM2Nzc3ODg2NywidXNlcl9pZCI6IjUzODIwNzE1MyJ9; hg_session[sessionId]=P7B1Smda7nydPuc1N85rLFJx0a339SZt; hg_session[sessionNum]=30; hg_session[startTime]=1367764475; ki_t=1366841895999%3B1367770645971%3B1367779161745%3B17%3B436; __utma=22815271.190081362.1366841896.1367770645.1367770645.44; __utmb=22815271.40.10.1367770680; __utmc=22815271; __utmz=22815271.1366841896.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)"
				//"Cookie" => 'PHPSESSID=k207njuepe0ianbffslubevkd1; ki_u=80e7178f-1986-a6d0-ef07-e57d102c9ad8; login_token=e69fa4b93be62075554e76e42ec968a7%7C538207153; ki_r=https%3A//www.facebook.com/; fbm_10337532241=base_domain=.mousehuntgame.com; bb_sessionhash=5ceba3f84a4ae0dae40554ddf49569f2; bb_thread_lastview=2f401b73472ec4c8bfae35aca4a2ee58c4683672a-1-%7Bi-90850_i-1367852883_%7D; hg_session[startTime]=1369028496; hg_session[sessionId]=NuQR6h0Raiw8gFhHFaP7ouYCySbAm476; hg_session[sessionNum]=73; ki_t=1368693972441%3B1369021822887%3B1369030047871%3B6%3B172; __utma=22815271.620497792.1368693973.1369021822.1369021822.13; __utmb=22815271.13.10.1369023452; __utmc=22815271; __utmz=22815271.1368693973.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
				//"Cookie"	=> 'PHPSESSID=fp94sp6cqi163mev92tcnoj1f0; ki_u=b09fcc66-2238-a65d-3a19-6e6ac4fc9ee6; login_token=e69fa4b93be62075554e76e42ec968a7%7C538207153; ki_r=http%3A//horntracker.com/scoreboard.php; fbm_10337532241=base_domain=.mousehuntgame.com; fbsr_10337532241=E8UBKvJGfV0JDLGwQFy1-V-SQMSYIJq6kjZXCQKfH9c.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImNvZGUiOiJBUUFIWmNsOW1GVHY1bzdSN3otaXpaaWpWaXZjcC1TWFhBRDJZUkJfVzJwTUZkb25XVm9IYzF5ajlycmswSlh0Ui10ZHp1d3ZHbVkxWnRDeVVaRThJZzV1b3BQM3BYUE03Q043Q0x0RTBwZ3VSTUZZNG1nblViTHRvN0VNZ0trU2x4bGJFbmNTZ0Y2dm55azUtWGNBRGQ0ZUxKZWlkOFR1OUdxSHQzQ2lfcEZjRUJjTWlUaVBwWktZR3RWbG1XWkc5dXppLTAzeEJ1VkVfMjJiVVE2LXp2TTNlWnRFS1dOTXhDYlF1cmNzUk9uSjNXVm85bFBXUVJYNUdDSC12S2JldGNxN2YxemVISHZWTUFydGhwYmJwaU9jdnRFMHRlOXJpTkdzMVpXNi10VVY0dVVoXzRhWFRUREFzOVZSMFJNUmF6RSIsImlzc3VlZF9hdCI6MTM3ODk5NDg2OSwidXNlcl9pZCI6IjUzODIwNzE1MyJ9; hg_session[sessionId]=68LB9SgvXWzG6pFl92fktOZgyN8kLGFh; hg_session[sessionNum]=60; hg_session[startTime]=1378966413; __utma=22815271.1911784691.1378579248.1378964598.1378999392.31; __utmb=22815271.6.10.1378999392; __utmc=22815271; __utmz=22815271.1378579248.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); ki_t=1378579244049%3B1378964599069%3B1379000459873%3B9%3B272'
				//"Cookie"	=> 'PHPSESSID=9hvqhpqd3qsum5j74dfpsdsvu4; ki_u=bcefec60-e0c5-6793-4feb-63c5dbdc9c55; hasprevauthed=1379580891; ki_r=https%3A//apps.facebook.com/mousehunt/%3Fhgref%3Dhud%26state%3Da2be53f5fc9d6571395ebde7b993b5a7%26code%3DAQBcJRZ9fpqZ0x4hkOQfIQeyLdA-WjcUZIqGzCwTpTnFwKhzG8jPleSVHB85yNX06LKLD0XXmonD6wDRR-h3vuiqPATgNRrBF_jFUYu9ctbRzMUOP-yD-75Yq5qPAp1mHvyLFKzmRwp6x89OkHW3zGh7Mp4xNh9Gj28p9b7CObgwjnLuyHlgmv1qTn2blIicaimTDnqCVpvmBZhaApZEf8j-JHInd9a6jkYYLgjpMC6etOqE60J-NlOaYrGtvU-WHCXr_kINRaqCj_8ZzO38ha1syuz_qpLgSrMfo4xvJF_lbp-qloSClgvDLIlE-xQwaeM; fbm_10337532241=base_domain=.mousehuntgame.com; hg_session[startTime]=1380001972; hg_session[sessionId]=BO39qObR84YW94Uq8BUk7eKrPlh66KI8; hg_session[sessionNum]=394; __utma=22815271.1980023935.1379526497.1379995823.1380002146.24; __utmb=22815271.7.10.1380002146; __utmc=22815271; __utmz=22815271.1379526497.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); ki_t=1379526495899%3B1380002146749%3B1380003612795%3B9%3B255'
				//"Cookie"	=> 'PHPSESSID=vfomk64dhfgdrrlsqfonelftg2; ki_u=774d9b9d-a5d6-b7cb-c907-dd73fe3ff367; fbm_10337532241=base_domain=.mousehuntgame.com; ki_r=https%3A//www.facebook.com/; bb_thread_lastview=9e79e459e9a34e1336ee69172e62b8b162f1a98aa-4-%7Bi-99436_i-1382284055_i-99547_i-1382306487_i-99701_i-1382304179_i-99648_i-1382304993_%7D; bb_sessionhash=de10587e6c9bc76d4e56af7f7310999a; ki_t=1382260826555%3B1382348593878%3B1382356903493%3B3%3B160; __utma=22815271.1123941709.1382260826.1382327849.1382335470.4; __utmb=22815271.113.9.1382356903894; __utmc=22815271; __utmz=22815271.1382260826.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); hg_session[startTime]=1382349276; hg_session[sessionId]=2Veetq05ObJI2wJzdFRxLaYfLZ5pd0UN; hg_session[sessionNum]=27'
				//"Cookie"	=> 'PHPSESSID=51p7fllq5cm3vou7tgu0uqkna4; ki_u=c4d93892-dcd3-6ee1-0e9b-3994cf506cc3; ki_r=https%3A//apps.facebook.com/mousehunt/%3Ffb_source%3Dbookmark; fbm_10337532241=base_domain=.mousehuntgame.com; bb_sessionhash=4f5a15d2ea34942ad465c2c1007c51dc; fbsr_10337532241=OAZuvC1uSvVOLzoceQ--ErsEVCuH2Ivz2Lzd9hQt-q8.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImNvZGUiOiJBUUJGSkRiVU5lbElkdFV2THg2RmI2MTR5dE42andFLWM3NFlKQjFWRkh3NjMzcURXcTdudVBJQUxfb1M3TVNtOFNrMkZrRFpNbHBJUVNqZkZrWFlzZHdMUEEyZzhzOHkzUE5OclNMdTA0YzhGU2FfcjFfTldJUzlDWjVDRHhLTUZjMEFPTzljc3NKTUNEUTFQS0d2blNDYzJEQm9uSTZxb0lwcWtKUDlvM3JIU1VkbzIyWlpXS1pjVGdsMzhzLWNXNWk1WFBRM295S0hiV3RvbkMzZW5NWl84blV3bndPMlp1bUQ2NVJxNWFEY3lZQjZrTFlWOWs4TE52S3V5YjY4Zmh3cDRnZWNnNkdmTUlGQlF1LTUycGxrMVF0OWkwMVRERmZMRHM3ZHAwdWVQeVBDRHAxUEZBWkhCQ0hVTkN0anZvayIsImlzc3VlZF9hdCI6MTM4Mzk5OTU5NywidXNlcl9pZCI6IjUzODIwNzE1MyJ9; hg_session[startTime]=1383992005; hg_session[sessionId]=C836TZK00s8JNG2nd0BF4F67w0lySD61; hg_session[sessionNum]=4; ki_t=1383855037237%3B1383995301647%3B1384000491148%3B4%3B80; __utma=22815271.603798083.1383855036.1383930545.1383995302.11; __utmb=22815271.26.10.1383995302; __utmc=22815271; __utmz=22815271.1383855036.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
				//"Cookie"	=> 'PHPSESSID=4jo7du5irefi2jfqe7qrr01ge4; ki_u=1a66916c-d0fe-8de1-42ef-934be7b4a35b; ki_r=https%3A//apps.facebook.com/mousehunt/%3Ffb_source%3Dnotification%26request_ids%3D556854447722271%26ref%3Dnotif%26app_request_type%3Duser_to_user%26notif_t%3Dapp_request; fbm_10337532241=base_domain=.mousehuntgame.com; hgRefHash=7OF75v0eOqgb02tyz2MBe0xk1hKoRZkO; hg_session[startTime]=1385133286; hg_session[sessionId]=Ooo9floydADiD68dBr1148FD7X4TS6oH; hg_session[sessionNum]=7; ki_t=1384371360296%3B1385115631982%3B1385143713822%3B17%3B803; __utma=22815271.824179709.1384371360.1385097253.1385137433.45; __utmb=22815271.54.9.1385143715055; __utmc=22815271; __utmz=22815271.1384371360.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'	
				//"Cookie"	=> 'PHPSESSID=8o3kp6p9n452q1ps750tailav3; ki_u=7bb089c5-fc38-5bf2-6c45-62318c4073f7; ki_r=https%3A//apps.facebook.com/mousehunt/index.php%3Fhg_sgh%3D6Ldvc958A5J5Z2TfRD91m456913julHqSLBUj6SK172HTp1KNLx63NV16t045g7r; fbm_10337532241=base_domain=.mousehuntgame.com; bb_thread_lastview=dc36e39860ea1c95e3d7efcb7061fc931936fe12a-1-%7Bi-101722_i-1386055656_%7D; bb_sessionhash=070edcaca5bba23ac04b5799442f1b06; hg_session[startTime]=1386492137; hg_session[sessionId]=AOTiFY8K8izniz3xHQurl3YmNWlUqk84; hg_session[sessionNum]=83; __utma=22815271.1147425179.1385928344.1386489676.1386492138.31; __utmb=22815271.26.10.1386492138; __utmc=22815271; __utmz=22815271.1385928344.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); ki_t=1385928340334%3B1386492138360%3B1386499080531%3B12%3B341'
				//"Cookie"	=> 'PHPSESSID=l93bsg4jlt26b58i5h3ovlkbl4; ki_u=9c92547e-9a07-32c6-69fd-ec7e2701e53f; ki_r=https%3A//www.facebook.com/l.php%3Fu%3Dhttps%253A%252F%252Fwww.mousehuntgame.com%252Fprofile.php%253Fsnuid%253D1051959075%26h%3DVAQGj7Kvc; hg_session[startTime]=1387222778; hg_session[sessionId]=Hos60vR0FUIStoNGF65767L1C52i7KWD; hg_session[sessionNum]=18; ki_t=1386781498116%3B1387222785764%3B1387226553792%3B5%3B91; __utma=22815271.1786600771.1386781497.1386916500.1387222785.8; __utmb=22815271.26.10.1387222785; __utmc=22815271; __utmz=22815271.1386781497.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
				//"Cookie"	=> 'PHPSESSID=djunu7134ud9uon7nppl41n4j6; ki_u=155c8d04-6fe1-ca9a-cde0-2f4dbc4c23da; ki_r=https%3A//apps.facebook.com/mousehunt/%3Ffb_source%3Dbookmark; fbm_10337532241=base_domain=.mousehuntgame.com; hg_session[startTime]=1387956660; hg_session[sessionId]=54532afTjI9wYRQUYbJ68YM8sFtd1pQh; hg_session[sessionNum]=25; ki_t=1387675026035%3B1387963453722%3B1387971019411%3B7%3B185; __utma=22815271.2004683701.1387675026.1387868055.1387963453.14; __utmb=22815271.39.9.1387971019583; __utmc=22815271; __utmz=22815271.1387675026.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
				//"Cookie"	=> 'PHPSESSID=mje39jkg9vau8ua2av9u2vcgu7; ki_u=2b31a252-71e5-f76e-2588-3bb3d15f107d; ki_r=https%3A//apps.facebook.com/mousehunt/%3Ffb_source%3Dbookmark; fbm_10337532241=base_domain=.mousehuntgame.com; ki_t=1388350929843%3B1388985880262%3B1389003139742%3B10%3B280; hg_session[startTime]=1388996514; hg_session[sessionId]=38vE6hbljMtxC03qSFV1mDK615pLuj71; hg_session[sessionNum]=50; __utma=22815271.1166605091.1388350934.1388680608.1388680608.24; __utmb=22815271.60.10.1388986849; __utmc=22815271; __utmz=22815271.1388350934.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
				//"Cookie"	=> 'PHPSESSID=mje39jkg9vau8ua2av9u2vcgu7; ki_u=2b31a252-71e5-f76e-2588-3bb3d15f107d; ki_r=https%3A//apps.facebook.com/mousehunt/%3Ffb_source%3Dbookmark; fbm_10337532241=base_domain=.mousehuntgame.com; hg_session[startTime]=1389077222; hg_session[sessionId]=4Tp7S6kaIDbfgdSjSARidDcjvcnZps8J; hg_session[sessionNum]=56; ki_t=1388350929843%3B1389042268727%3B1389079094586%3B11%3B317; __utma=22815271.1166605091.1388350934.1389042268.1389042268.28; __utmb=22815271.11.10.1389071908; __utmc=22815271; __utmz=22815271.1388350934.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
				//"Cookie"	=> 'PHPSESSID=i1ni5jk1fjnitdth79qqi3ts50; ki_u=cdf02a75-c8ed-f31a-955a-3360a06b86f4; ki_r=https%3A//apps.facebook.com/mousehunt/huntersgroup.php%3Frefresh%3D1; fbm_10337532241=base_domain=.mousehuntgame.com; hg_session[startTime]=1389681212; hg_session[sessionId]=WmQlyWC7s4MpXo52G363fIoZt5c4Y83P; hg_session[sessionNum]=3; __utma=22815271.1910310421.1389211062.1389608002.1389608002.23; __utmb=22815271.2.10.1389676806; __utmc=22815271; __utmz=22815271.1389211062.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); ki_t=1389211062794%3B1389676782199%3B1389681902048%3B7%3B159'
				// "Cookie"	=> 'PHPSESSID=8a5rbjuu34ls6sboje37ijtoo4; ki_u=8ce176cf-0d56-9da4-f064-2c01b980c704; ki_r=https%3A//apps.facebook.com/mousehunt/%3Ffb_source%3Dbookmark; fbm_10337532241=base_domain=.mousehuntgame.com; hg_session[startTime]=1389908445; hg_session[sessionId]=z4s8OLp43jOilxhuSs82qFl3nwexPmRk; hg_session[sessionNum]=85; __utma=22815271.858546367.1389814456.1389935585.1389941891.10; __utmb=22815271.97.10.1389941891; __utmc=22815271; __utmz=22815271.1389814456.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); ki_t=1389814456175%3B1389946662313%3B1389969632989%3B4%3B70'
// 				"Cookie" => 'PHPSESSID=v98767ndpsjjng3rh0a2t7cgm3; hg_session[startTime]=1432009935; hg_session[sessionId]=wvWcDCu6EVyYgmqEeF2v64v7czG10dsQ; hg_session[sessionNum]=30; __utmt=1; __utma=22815271.1021342443.1431882685.1431964114.1431964114.4; __utmb=22815271.41.9.1432013190142; __utmc=22815271; __utmz=22815271.1431882685.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
				"Cookie" => 'PHPSESSID=brbogaqovjjvqvkgdraehtv8s6; hideNotOwnedSellItems=true; __utmt=1; hg_session[startTime]=1444320337; hg_session[sessionId]=z8vg1qj6l631X7Hl2tq9l40hE5nWoeuE; hg_session[sessionNum]=3; __utma=22815271.1563936386.1443854101.1444103704.1444330457.14; __utmb=22815271.17.9.1444332589270; __utmc=22815271; __utmz=22815271.1443854101.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
		);
		
		foreach($headers as $name => $content) {
			$this->curl->http_header($name, $content);
		}
		
		if($this->user == 'lucia') {
			$a = $this->curl->ssl(TRUE, 2, data_url() . "cacert.pem");
		}
		
		$this->curl->create($url);
		$raw = $this->curl->execute();
		
//		$this->curl->debug();
		
		$data = json_decode($raw);	//user, messageData, badges, favorites, mouse_data, remaining_mice, is_viewing_user, success
		return $data;
	}
	
	private function execute_lucia($action, $snuid) {
	
		$url = "https://www.mousehuntgame.com/managers/ajax/mice/getstat.php?action=getMiceByGroup&fetch_categories=false&category=forgotten&snuid=$snuid&uh=x0D6F716&hg_is_ajax=1";
	
		$headers = array(
				"Accept"			=> "text/html",
				"Cookie"	=> 'PHPSESSID=mje39jkg9vau8ua2av9u2vcgu7; ki_u=2b31a252-71e5-f76e-2588-3bb3d15f107d; ki_r=https%3A//apps.facebook.com/mousehunt/%3Ffb_source%3Dbookmark; fbm_10337532241=base_domain=.mousehuntgame.com; ki_t=1388350929843%3B1388985880262%3B1389003139742%3B10%3B280; hg_session[startTime]=1388996514; hg_session[sessionId]=38vE6hbljMtxC03qSFV1mDK615pLuj71; hg_session[sessionNum]=50; __utma=22815271.1166605091.1388350934.1388680608.1388680608.24; __utmb=22815271.60.10.1388986849; __utmc=22815271; __utmz=22815271.1388350934.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
		);
	
		foreach($headers as $name => $content) {
			$this->curl->http_header($name, $content);
		}
	
		$this->curl->ssl(TRUE, 2, data_url() . "cacert.pem");
	
		$this->curl->create($url);
		$raw = $this->curl->execute();
	
		$data = json_decode($raw);	//user, messageData, badges, favorites, mouse_data, remaining_mice, is_viewing_user, success
		return $data;
	}
	
	
	
	//PHPSESSID=j6va9mmukqlij47sn6c9pr4kn5; ki_u=177e5720-1aec-30f4-51cd-b92cbef507a3; ki_r=https%3A//apps.facebook.com/mousehunt/%3Ffb_source%3Dnotification%26request_ids%3D557510744331356%252C443806699058332%26ref%3Dnotif%26app_request_type%3Duser_to_user%26notif_t%3Dapp_request; fbm_10337532241=base_domain=.mousehuntgame.com; ki_t=1385318912839%3B1385405988444%3B1385406123142%3B3%3B93; __utma=22815271.1066579119.1385318913.1385355502.1385396457.4; __utmb=22815271.68.10.1385396457; __utmc=22815271; __utmz=22815271.1385318913.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); hg_session[startTime]=1385368708; hg_session[sessionId]=Q14WOvwMc18CW2xtliq9oBYT0R9ZzNbl; hg_session[sessionNum]=8
	public function execute_test($snuid='538207153') {

		$url = "https://www.mousehuntgame.com/managers/ajax/mice/getstat.php?action=getMiceByGroup&fetch_categories=false&category=forgotten&snuid=$snuid&uh=x0D6F716&hg_is_ajax=1&sn=FBConnect";
		
		$headers = array(
				"Accept"			=> "text/html",
				"Cookie"	=> 'PHPSESSID=4jo7du5irefi2jfqe7qrr01ge4; ki_u=1a66916c-d0fe-8de1-42ef-934be7b4a35b; ki_r=https%3A//apps.facebook.com/mousehunt/%3Ffb_source%3Dnotification%26request_ids%3D556854447722271%26ref%3Dnotif%26app_request_type%3Duser_to_user%26notif_t%3Dapp_request; fbm_10337532241=base_domain=.mousehuntgame.com; hgRefHash=7OF75v0eOqgb02tyz2MBe0xk1hKoRZkO; hg_session[startTime]=1385133286; hg_session[sessionId]=Ooo9floydADiD68dBr1148FD7X4TS6oH; hg_session[sessionNum]=7; ki_t=1384371360296%3B1385115631982%3B1385143713822%3B17%3B803; __utma=22815271.824179709.1384371360.1385097253.1385137433.45; __utmb=22815271.54.9.1385143715055; __utmc=22815271; __utmz=22815271.1384371360.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'	
		);
		
		foreach($headers as $name => $content) {
			$this->curl->http_header($name, $content);
		}
		
		$this->curl->ssl(TRUE, 2, data_url() . "cacert.pem");
		
		$this->curl->create($url);
		$raw = $this->curl->execute();
		
		//$data = json_decode($raw);	//user, messageData, badges, favorites, mouse_data, remaining_mice, is_viewing_user, success
		header('Content-Type: application/json charset=utf-8');
		echo $raw;
		//echo $data;
	}
	
	
	private function lucia($snuidArr) {
	
		$this->start_data = $this->parse_csv2( APPPATH . 'lucia.csv');
		
		//user, messageData, categories, category, miceStat, success
		$this->load->model('mh_db');
		$this->saved_data = $this->mh_db->get_customer_data('lucia');
		//echo "<pre>"; print_r($start_data); echo "</pre>"; die();
		
		$return = array(); $i = 0;
		foreach($snuidArr as $snuid) {
				
			$data = $this->execute_lucia('', $snuid);
			
			$snuid = (string) $snuid;
			foreach($this->start_data as $key => $hunter) {
				if($hunter['snuid'] == $snuid) break;
			}
			$start = $this->start_data[$key];
			
			$mice = get_object_vars($data->miceStat);
			
			$temp = array();
			foreach($mice as $id => $mouse) {
				if($mouse->name == "Acolyte Mouse") {
					$caught = ( (int) trim($mouse->caught) ) - ( (int) trim($start['acolyte_catch']) );
					$missed = ( (int) trim($mouse->missed) ) - ( (int) trim($start['acolyte_miss']) );
					
					$temp[] = array($mouse->name, "acolyte", $caught . " / " .$missed);
				}
				else if($mouse->name == "Chrono Mouse") {
					$caught = ( (int) trim($mouse->caught) ) - ( (int) trim($start['chrono_catch']) );
					$missed = ( (int) trim($mouse->missed) ) - ( (int) trim($start['chrono_miss']) );
						
					$temp[] = array($mouse->name, "chrono", $caught . " / " .$missed);
				}
				else if($mouse->name == "Lich Mouse") {
					$caught = ( (int) trim($mouse->caught) ) - ( (int) trim($start['lich_catch']) );
					$missed = ( (int) trim($mouse->missed) ) - ( (int) trim($start['lich_miss']) );
					
					$temp[] = array($mouse->name, "lich", $caught . " / " .$missed);
				}
			}
			$temp[] = array('POINTS', "POINTS", $this->points($snuid) );
			$return[] = array('snuid' => $snuid, 'crowns' => $temp, 'items' => array());
			$i++;
			break;
		}

		header('Content-Type: application/json charset=utf-8');
		echo json_encode($return);
		//return $return;
	}
	
	private function points($snuid) {
		
		$debug = 0;
		
		//komaj zaceli
		if(count($this->saved_data) == 0) return 0;
		
		$snuid = (string) $snuid;
		foreach($this->start_data as $key => $hunter) {
			if($hunter['snuid'] == $snuid) break;
		}
		
		$start = $this->start_data[$key];
		$sad = $this->saved_data[$snuid];
		
		$aki_sad = explode('/', $sad['crowns']['acolyte']);
		$chrono_sad = explode('/', $sad['crowns']['chrono']);
		$lich_sad = explode('/', $sad['crowns']['lich']);
		
		/*
		 if($debug == 1) {
		echo "<pre>"; print_r($aki_sad[0]); echo "</pre>";
		echo "<pre>"; print_r($start['acolyte_catch']); echo "</pre>";
		
		echo "<pre>"; print_r($aki_sad[1]); echo "</pre>";
		echo "<pre>"; print_r($start['acolyte_miss']); echo "</pre>";
		}
		*/
		
		//$aki_points = ( ( (int) trim($aki_sad[0]) ) - ( (int) trim($start['acolyte_catch']) ) )*15 - ( ( (int) trim($aki_sad[1]) ) - ( (int) trim($start['acolyte_miss']) ) )*10;
		$aki_points = ( (int) trim($aki_sad[0]) )*15 - ( (int) trim($aki_sad[1]) )*10;
		if($debug == 1) echo "AKI: $aki_points".BR;
		
		//$chrono_points = ( ( (int) trim($chrono_sad[0]) ) - ( (int) trim($start['chrono_catch']) ) )*30 - ( ( (int) trim($chrono_sad[1]) ) - ( (int) trim($start['chrono_miss']) ) )*25;
		$chrono_points = ( (int) trim($chrono_sad[0]) )*30 - ( (int) trim($chrono_sad[1]) )*25;
		if($debug == 1) echo "CHRONO: $chrono_points".BR;
		
		//$lich_points = ( ( (int) trim($lich_sad[0]) ) - ( (int) trim($start['lich_catch']) ) )*5;
		$lich_points = ( (int) trim($lich_sad[0]) )*5;
		if($debug == 1) echo "LICH: $lich_points".BR;
		
		$points = $aki_points + $chrono_points + $lich_points;
		if($debug == 1) echo "TOTAL: $points".BR;
		
		return $points;
	}
	
	private function parse_csv2($file) {
	
		//http://php.net/manual/en/function.fgetcsv.php
		$row = 1; $return = array();
		if (($handle = fopen($file, "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$return[] = array(
						'name' => $data[0],
						'link' => $data[1],
						'snuid' => substr($data[1], strpos($data[1], '=')+1),
						'chrono_catch' 	=> $data[2],
						'chrono_miss'	=> $data[3],
						'acolyte_catch'	=> $data[4],
						'acolyte_miss'	=> $data[5],
						'lich_catch'	=> $data[6],
						'lich_miss'		=> $data[7]
				);
				$row++;
			}
			fclose($handle);
		}
		return $return;
	}

	//DEPRECATED
	private function init() {
		
		//tournament_badge_challenger_collectible, tournament_badge_competitor_collectible, tournament_badge_participant_collectible
		$data = array(
				
			'bama' => array(
					'collectibles' => array(),
					'crowns'	=> array(),
					'actions'	=> array('collectibles', 'crowns')
					),
			'mhcc' => array(
					'collectibles' => array(),
					'crowns'	=> array(),
					'actions'	=> array('collectibles', 'crowns')
					),
			'vermin' => array(
					'collectibles' 	=> array('tournament_trophy_gold_collectible', 'tournament_trophy_silver_collectible', 'tournament_trophy_bronze_collectible'),
					'crowns'		=> array('eclipse', 'desert_boss', 'chess_master', 'balack_the_banished', 'dragon', 'acolyte', 'dojo_sensei', 'silth', 'library_boss'),
					'actions'	=> array('crowns', 'collectibles')
					)
		);

		return $data[$this->customer];
	}
	
	
	public function teams() {
		
		$url = "http://www.mousehuntgame.com/team.php?team_id=32161";
		
		$headers = array(
				"Accept" => "text/html",
				//"Cookie" => "PHPSESSID=mffrr3auc8vjf57j0i0ngt9ko4; ki_u=3f4bff47-bc8f-c4d9-e092-d6ae426beb6e; login_token=e69fa4b93be62075554e76e42ec968a7%7C538207153; ki_r=https%3A//www.facebook.com/l.php%3Fu%3Dhttps%253A%252F%252Fwww.mousehuntgame.com%252Fitem.php%253Fitem_type%253Dmap_clue_stat_item%26h%3D8AQGPme5G; fbm_10337532241=base_domain=.mousehuntgame.com; bb_sessionhash=d5e70e62d2028e2c67da1cf89c9ea3c8; fbsr_10337532241=jT0fy--8RnF3X5xPU0yz_OgxgvVkMjH-GniANGmEgIU.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImNvZGUiOiJBUUR5WXlOWjFVczZ1MHN5Mjc0bV9qWkVlSnFkUTd3ZE1ObGh4cF9WRUc3M2g3Z25Db1pJR2NWUUtJdUt1aUdVUG9GQWlyWFhyR0ZlRDVQb0JPZl9WN2MwSjR3em1RSkpiV24ybmJOOXlTRk00YUlWUXU1R3I5RFl5TWdOaFFQZUhITTR1MWNJTmx6clhPNGdDbVZPUXF3ckJFd0hEcEtpaTdNVGFYT01tRHFuWkEyWGhfcHc4UC1JS0RYVlFIbS1wb0diN1dsbDFPeGstbHNIeF9KQi12dXVzVGpMbWlkb3JDaXFXdmJWN2U2OXV4Tk1RRG1aelF0R1p6MElLeUN1WFVkcEtlR1dKZ2ltS2RoX2RvNm94SmE0REFucTFSNFdkVVVOQlNaY00zc0ZjeFRTRlB3dlhNRU5fZEs4M1VkREZpRSIsImlzc3VlZF9hdCI6MTM2Nzc3ODg2NywidXNlcl9pZCI6IjUzODIwNzE1MyJ9; hg_session[sessionId]=P7B1Smda7nydPuc1N85rLFJx0a339SZt; hg_session[sessionNum]=30; hg_session[startTime]=1367764475; ki_t=1366841895999%3B1367770645971%3B1367779161745%3B17%3B436; __utma=22815271.190081362.1366841896.1367770645.1367770645.44; __utmb=22815271.40.10.1367770680; __utmc=22815271; __utmz=22815271.1366841896.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)"
				"Cookie" => 'PHPSESSID=k207njuepe0ianbffslubevkd1; ki_u=80e7178f-1986-a6d0-ef07-e57d102c9ad8; login_token=e69fa4b93be62075554e76e42ec968a7%7C538207153; ki_r=https%3A//www.facebook.com/; fbm_10337532241=base_domain=.mousehuntgame.com; bb_sessionhash=5ceba3f84a4ae0dae40554ddf49569f2; bb_thread_lastview=2f401b73472ec4c8bfae35aca4a2ee58c4683672a-1-%7Bi-90850_i-1367852883_%7D; hg_session[startTime]=1369028496; hg_session[sessionId]=NuQR6h0Raiw8gFhHFaP7ouYCySbAm476; hg_session[sessionNum]=73; ki_t=1368693972441%3B1369021822887%3B1369030047871%3B6%3B172; __utma=22815271.620497792.1368693973.1369021822.1369021822.13; __utmb=22815271.13.10.1369023452; __utmc=22815271; __utmz=22815271.1368693973.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
		);
		
		foreach($headers as $name => $content) {
			$this->curl->http_header($name, $content);
		}
		
		$this->curl->ssl(TRUE, 2, data_url() . "cacert.pem");
		
		
		
		$this->curl->create($url);
		$raw = $this->curl->execute();
		
		$data = json_encode($raw);
		
		header('Content-Type: application/json charset=utf-8');
		echo $data;
		
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
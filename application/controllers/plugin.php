<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Plugin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		//$this->load->helper( array('url', 'general') );
	}
	
	public function maps_tweeker() {
		$locations = $this->locations();
		
		$return = array("status" => "ok");
		
		$return['data'] = $locations;
		
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json charset=utf-8');
		echo json_encode($return);
		
	}
	
	private function locations() {
		$locations = array(
			"Meadow" => array("Field Mouse", "Flying Mouse", "Spotted Mouse", "Tiny Mouse"),
			"Town of Gnawnia" => array("Brown Mouse", "Cowardly Mouse", "Dwarf Mouse", "Grey Mouse", "Master Burglar Mouse", "Nibbler Mouse", "White Mouse"),
			"Windmill" => array("Spud Mouse"),
			"Harbour" => array("Magic Mouse", "Pirate Mouse"),
			"Mountain" => array("Black Widow Mouse", "Diamond Mouse", "Gold Mouse", "Silvertail Mouse"),
			"Slushy Shoreline" => array("Living Ice Mouse", "Yeti Mouse"),
			"King&#8217;s Arms" => array(),
			"Tournament Hall" => array("Lightning Rod Mouse"),
			"King&#8217;s Gauntlet" => array(),
			"Calm Clearing" => array("Bear Mouse", "Frog Mouse", "Moosker Mouse"),
			"Great Gnarled Tree" => array(),
			"Lagoon" => array("Centaur Mouse", "Elven Princess Mouse", "Hydra Mouse", "Nomad Mouse", "Tiger Mouse"),
			"Claw Shot City" => array(),
			"Laboratory" => array("Monster Mouse", "Steel Mouse"),
			"Town of Digby" => array("Bionic Mouse", "Granite Mouse", "Zombie Mouse"),
			"Mousoleum" => array("Bat Mouse", "Mummy Mouse", "Ravenous Zombie Mouse"),
			"Bazaar" => array("Burglar Mouse"),
			"Training Grounds" => array("Monk Mouse", "Ninja Mouse", "Worker Mouse"),
			"Dojo" => array("Assassin Mouse", "Student of the Cheese Belt Mouse", "Student of the Cheese Claw Mouse", "Student of the Cheese Fang Mouse"),
			"Meditation Room" => array("Hapless Mouse", "Master of the Cheese Belt Mouse", "Master of the Cheese Claw Mouse", "Master of the Cheese Fang Mouse"),
			"Pinnacle Chamber" => array("Dojo Sensei", "Master of the Dojo Mouse"),
			"Catacombs" => array("Giant Snail Mouse", "Keeper Mouse", "Ooze Mouse", "Scavenger Mouse", "Terror Knight Mouse"),
			"Forbidden Grove" => array("Lycan Mouse", "Mutated Grey Mouse", "Mutated White Mouse", "Realm Ripper", "Vampire Mouse"),
			"Acolyte Realm" => array("Acolyte Mouse", "Lich Mouse"),
			"S.S. Huntington III" => array("Briegull Mouse", "Siren Mouse", "Swabbie Mouse", "Mermouse"),
			"Seasonal Garden" => array("Harvest Harrier Mouse", "Icicle Mouse", "Puddlemancer Mouse", "Stinger Mouse"),
			"Zugzwang&#8217;s Tower" => array("Chess Master", "Mystic Bishop Mouse", "Mystic King Mouse", "Mystic Rook Mouse", "Technic Bishop Mouse", "Technic King Mouse", "Technic Rook Mouse"),
			"Crystal Library" => array("Aether Mouse", "Effervescent Mouse", "Flutterby Mouse", "Infiltrator Mouse", "Pocketwatch Mouse", "Walker Mouse"),
			"Iceberg" => array("Chipper Mouse", "General Drheller", "Iceblade Mouse", "Lady Coldsnap", "Living Salt Mouse", "Lord Splodington", "Polar Bear Mouse", "Princess Fist", "Snow Slinger Mouse", "Stickybomber Mouse"),
			"Cape Clawed" => array("Aged Mouse"),
			"Elub Shore" => array("Elub Chieftain Mouse", "Mystic Mouse", "Pinchy Mouse", "Soothsayer Mouse"),
			"Nerg Plains" => array("Chameleon Mouse", "Conjurer Mouse", "Conqueror Mouse", "Nerg Chieftain Mouse"),
			"Derr Dunes" => array("Derr Chieftain Mouse", "Mintaka Mouse", "Seer Mouse"),
			"Jungle of Dread" => array("Chitinous Mouse", "Fetid Swamp Mouse", "Jurassic Mouse", "Magma Carrier Mouse", "Primal Mouse", "Pygmy Wrangler Mouse", "Stonework Warrior Mouse", "Swarm of Pygmy Mice", "Sylvan Mouse"),
			"Dracano" => array(),
			"Balack&#8217;s Cove" => array("Balack the Banished", "Brimstone Mouse", "Derr Lich Mouse", "Elub Lich Mouse", "Nerg Lich Mouse", "Riptide Mouse", "Twisted Fiend Mouse"),
			"Fiery Warpath" => array("Magmarage Mouse", "Sand Cavalry Mouse"),
			"Muridae Market" => array("Blacksmith Mouse", "Desert Architect Mouse", "Falling Carpet Mouse", "Mage Weaver Mouse", "Pie Thief Mouse", "Snake Charmer Mouse", "Spice Merchant Mouse"),
			"Living Garden" => array("Thirsty Mouse", "Thistle Mouse", "Strawberry Hotcakes Mouse", "Bark Mouse", "Calalilly Mouse", "Shroom Mouse", "Camoflower Mouse", "Carmine the Apothecary"),
			"Lost City" => array("Cursed Mouse", "Essence Collector Mouse", "Ethereal Enchanter Mouse", "Ethereal Engineer", "Ethereal Librarian", "Ethereal Thief Mouse"),
			"Sand Dunes" => array("Dunehopper Mouse", "Grubling Mouse", "Grubling Herder Mouse", "Quesodillo Mouse", "Sand Pilgrim", "Spiky Devil Mouse"),
			"Twisted Garden" => array("Dehydrated Mouse", "Thorn Mouse", "Twisted Hotcakes Mouse", "Barkshell Mouse", "Twisted Lilly Mouse", "Fungal Spore Mouse", "Camofusion Mouse", "Twisted Carmine"),
			"Cursed City" => array("Essence Guardian Mouse", "Cursed Enchanter Mouse", "Corrupt Mouse", "Cursed Engineer Mouse", "Cursed Thief Mouse", "Cursed Librarian Mouse"),
			"Sand Crypts" => array("Sarcophamouse", "Sand Colossus Mouse", "Serpentine Mouse", "Scarab Mouse", "King Grub Mouse"),
			//GWH 2013
			"Festive Snow Fort" => array("Snowflake Mouse", "Nutcracker Mouse", "Candy Cane Mouse", "Snow Scavenger", "Snowglobe Mouse", "Ridiculous Sweater Mouse", "Triple Lutz Mouse", "Destructoy Mouse", "Snowblower Mouse", "Elf Mouse", "Missile Toe Mouse", "Wreath Thief Mouse", "Snow Fort Mouse")
		);
		
		return $locations;
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/plugin.php */
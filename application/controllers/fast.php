<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fast extends CI_Controller {

	public $debug = FALSE;
	public $hunters;
	private $mice;
	private $customer = '';
	private $comp_mice;
	private $categories;
	
	public function __construct() {
		parent::__construct();
		$this->load->library( array('curl') );
		$this->load->helper( array('url', 'general', 'language') );
		
		//load mice array
		$this->set_mice_array();
		
		//load comp mice
		$this->set_comp_mice();
		
		$this->hunters = $this->parse_csv( data_url() . 'comp.csv' );
		
		log_message('debug', "Main Controller Class Initialized");
	}
	
	public function index($start=0) {

		$start = (int) $start;
		$this->benchmark->mark('code_start');
		$this->parser($start);
		$this->benchmark->mark('code_end');
	}
	

	public function parser($start) {
		
		$this->load->library('table');
		
		$tmpl = array ( 'table_open' => '<table class="table table-striped">' );
		$this->table->set_template($tmpl);
		
		$heading = array('#', 'Name');
		$heading_mice = array(0 => '', 1 => '');
		
		//we have IDs but we want mice names
		foreach($this->comp_mice as $key) {
			$cellKeys[] = $this->mice[$key];
			$heading[] = '<img src="'.images_url().'mice/'. $key.'.gif" alt="'.$this->mice[$key].'" title="'.$this->mice[$key].'" width="50px" height="50px" />';
			$heading_mice[] = $key;
		}
		
		$this->table->set_heading($heading);
		
		//get saved data for current customer
		$snuids = array();

		//slice
		$output = array_slice($this->hunters, $start, 20);
		
		//collect data only for current page
		foreach($output as $count => $hunter) {
			
			$snuids[] = $snuid = $hunter['snuid'];
			
			$parsed_data = array();
			
			
			//###### PARSING AREA ##################
			
			foreach($this->categories as $cat_id => $cat) {
				
				echo "$snuid | $cat_id | $cat".BR;
				
				$data = $this->execute($cat, $snuid);
				
				$mice = get_object_vars($data->miceStat);
				
				$temp = array();
				foreach($mice as $id => $mouse) {
					
					//najdi opisno ime
					$name_long = (string) trim($mouse->name);
					
					//najdi kratko ime v listi vseh misi
					$name_short = array_search($name_long, $this->mice);
					
					if($name_short !== FALSE && in_array($name_short, $this->comp_mice)) {
						$parsed_data[$name_short] = (string) $mouse->caught;
					}
				}
				
				//TESTING
				//if($cat_id > 0) break;
			}
			
			//############## DISPLAY AREA ###################
			
			$row = array();
			//add rows
			for($i=0; $i<count($heading); $i++) {

				if($i == 0) {
					$row[] = array('data' => $count);
				}
				else if($i==1) {
					$row[] = (array('data' => anchor($hunter['link'], $hunter['name'], array('target' => 'blank', 'title' => 'open hunter link in new window'))));
				}
				else {
					$pointer = $heading_mice[$i];
					$val = (isset($parsed_data[$pointer])) ? $parsed_data[$pointer] : 'n/a';
					$row[] = array('data' => $val);
				}
				
			}
			
			$this->table->add_row($row);
			
			//TESTING
			//if($count > 0) break;
			
		}
		
		$table = $this->table->generate();
		
		$outData = array(
				'table' => $table,
				'snuids'=> implode(',', $snuids)
			);
		
		$this->output('main/parser_view', NULL, $outData, false );
	}
	
	private function execute($cat, $snuid) {
	
		$url = "https://www.mousehuntgame.com/managers/ajax/mice/getstat.php?action=getMiceByGroup&fetch_categories=false&category=$cat&snuid=$snuid&uh=x0D6F716&hg_is_ajax=1&sn=FBConnect";
	
		$headers = array(
				"Accept"	=> "text/html",
				"Cookie"	=> 'PHPSESSID=0hn2494ugmodrn59gbfsnaj2q1; ki_u=63417af3-eec4-ef2d-91f5-74c3e25d73e1; login_token=e69fa4b93be62075554e76e42ec968a7%7C538207153; ki_r=https%3A//apps.facebook.com/mousehunt/%3Ffb_source%3Dnotification%26request_ids%3D198062420347267%26ref%3Dnotif%26app_request_type%3Duser_to_user%26notif_t%3Dapp_request; fbm_10337532241=base_domain=.mousehuntgame.com; hg_session[startTime]=1370498442; hg_session[sessionId]=oaibulx6I4J7z8L6MdeR64jzxzY0ubw0; hg_session[sessionNum]=33; ki_t=1370505683971%3B1370505683971%3B1370523947891%3B1%3B21; __utma=22815271.374945668.1370505686.1370505686.1370505686.2; __utmb=22815271.14.10.1370515211; __utmc=22815271; __utmz=22815271.1370505686.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)'
		);
	
		foreach($headers as $name => $content) {
			$this->curl->http_header($name, $content);
		}
	
		$a = $this->curl->ssl(TRUE, 2, data_url() . "cacert.pem");
		
		$this->benchmark->mark('pocetak');
		$this->curl->create($url);
		$raw = $this->curl->execute();
		$this->benchmark->mark('kraj');
		
		//echo "CURL: ". $this->benchmark->elapsed_time('pocetak', 'kraj').BR;
	
		$data = json_decode($raw);	//user, messageData, badges, favorites, mouse_data, remaining_mice, is_viewing_user, success
		return $data;
	}

	private function curl($url, $ssl = FALSE ) {
	
		if(!isset($url) || $url=='') return '';
	
		if(!isset($this->curl)) $this->load->library('curl');
	
		$c = $this->curl;
		$c->create($url);
	
		$c->option(CURLOPT_FAILONERROR, true);
		$c->option(CURLOPT_CONNECTTIMEOUT, 30); //The number of seconds to wait while trying to connect. Use 0 to wait indefinitely.
		$c->option(CURLOPT_TIMEOUT, 120);	//The maximum number of seconds to allow cURL functions to execute.
		$c->option(CURLOPT_RETURNTRANSFER, TRUE);
		$c->option(CURLOPT_ENCODING, 'gzip');
	
		if($ssl) $a = $this->curl->ssl(TRUE, 2, data_url() . "cacert.pem");
	
		//special options
		$c->option(CURLOPT_TIMECONDITION, CURL_TIMECOND_IFMODSINCE);
		$response = $c->execute();
		if($this->debug) {
			$c->debug();
		}
		return $response;
	}
	
	private function output($pageName, $pageTitle='CMS', $data=array(), $include_menu=true) {
	
		$data['title'] = $pageTitle;
	
		//nafilamo podatke za globalni meni. Ce imamo breadcrumbe, jih prikazemo (po levelih)
		$menuData = array(
				'title' 		=> $pageTitle,
				'user_group'	=> '',
				'breadcrumbs' 	=> (isset($data['breadcrumbs'])) ? $data['breadcrumbs'] : 0
		);
	
		$this->load->view('template/header');
		if($include_menu) $this->load->view('template/menu', $menuData );
		$this->load->view($pageName, $data);
		$this->load->view('template/footer');
	}
	
	//init func
	private function set_mice_array() {
		
		if (defined('ENVIRONMENT') AND file_exists(APPPATH.'config/'.ENVIRONMENT.'/bama_mice.php')) {
		    include(APPPATH.'config/'.ENVIRONMENT.'/bama_mice.php');
		}
		elseif (file_exists(APPPATH.'config/bama_mice.php')) {
			include(APPPATH.'config/bama_mice.php');
		}

		//put into global
		$this->mice = (isset($mice) AND is_array($mice)) ? $mice : array();
	}
	
	private function set_comp_mice() {
		
		$this->comp_mice = array(
				'realm_guardian',
				//dread
				'chitinous', 'fetid_swamp', 'jurassic', 'magma_carrier', 'primal', 'stonework_warrior', 'pygmy_wrangler',
				//SG
				'vinetail', 'harvester', 'summer_mage', 'winter_mage',
				//ZT
				'tech_king', 'mystic_king', 'chess_master',
				//FW
				'desert_beast', 'desert_general',
				//Iceberg
				'lady_coldsnap', 'drheller', 'lord_splodington', 'princess_fist', 'icewing',
				//Digby
				'nugget', 'big_bad_burroughs',
				//AR
				'acolyte',
				//BC
				'riptide'
				);
		
		//dodatno: common, gauntlet, forest, shadow, furoma, hydro, elub, nerg, derr, dracaonic, desertmarket, zzlibrary, living_garden, lost_city, sand_dunes, event
		$this->categories = array('dread', 'dirt', 'forgotten', 'balack', 'seasonal', 'chess', 'desertarmy', 'iceberg');
		
	}
	
	private function parse_csv($file) {
	
		//http://php.net/manual/en/function.fgetcsv.php
		$row = 1; $return = array();
		if (($handle = fopen($file, "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$return[] = array( 'name' => $data[0], 'link' => $data[1], 'snuid' => substr($data[1], strpos($data[1], '=')+1) );
				$row++;
			}
			fclose($handle);
		}
		return $return;
	}
	

	
	
	
	
	
	
	
	
	
	
	
	

}

/* End of file comp.php */
/* Location: ./application/controllers/comp.php */